<% if (~props.box.indexOf('debian')) { %>
echo "Installing curl"
sudo apt-get install curl
echo "Adding nodejs repo to sources"
curl -sL https://deb.nodesource.com/setup | sudo bash -
echo "Installing NodeJS"
sudo apt-get install -y nodejs
echo "Installing build tools"
sudo apt-get install -y build-essential
echo "Make NPM -G usable"
npm config set prefix ~

echo "Installing NPM modules"
npm install --global <%= props.node_modules.join(' ') %>
echo "Installing extra NPM modules"
npm install --global <%= props.extra_node_modules %>
<% } %>
