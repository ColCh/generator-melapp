'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');

var MelappGenerator = yeoman.generators.Base.extend({
  initializing: function () {
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the vagrant generator!'
    ));

    var prompts = [
      {
        type: 'input',
        name: 'box',
        message: 'Enter boxname (https://vagrantcloud.com/)',
        default: 'chef/debian-7.4'
      },
      {
        type: 'input',
        name: 'cpus',
        message: 'Enter number of CPU cores for VM',
        default: '1'
      },
      {
        type: 'input',
        name: 'mem',
        message: 'Enter desired memory amount (MB)',
        default: '1024'
      },
      {
        type: 'confirm',
        name: 'intel_virt',
        message: 'Include intel virt options to VM?',
        default: true
      },
      {
        type: 'confirm',
        name: 'foodshow',
        message: 'Use ngrok foodshow?',
        default: true
      },
      {
        type: 'checkbox',
        name: 'node_modules',
        message: 'Select common node packages to install globally?',
        choices: [
          { value: 'koa', checked: false },
          { value: 'express', checked: true },
          { value: 'http-server', checked: true }
        ]
      },
      {
        type: 'input',
        name: 'extra_node_modules',
        message: 'Input additional modules. Split by " " (space)?'
      },
      {
        type: 'confirm',
        name: 'vagrant_up',
        message: 'Run `vagrant up` on exit?',
        default: true
      },
    ];

    this.prompt(prompts, function (props) {

      this.props = props;

      done();
    }.bind(this));
  },

  writing: {
    vagrantfile: function () {
      this.template('_vagrantfile', 'Vagrantfile');
      this.template('_provision.sh', 'provision.sh');
    }
  },

  end: function () {
    if (this.props.vagrant_up) {
      this.log('Running vagrant up');
      this.spawnCommand('vagrant', ['up']);
    }
  }
});

module.exports = MelappGenerator;
